local cane_ancient_fx   = GetModConfigData("CANE_ANCIENT_FX")
local cane_victorian_fx = GetModConfigData("CANE_VICTORIAN_FX")
local cane_candy_fx     = GetModConfigData("CANE_CANDY_FX")
local cane_sharp_fx     = GetModConfigData("CANE_SHARP_FX")
local cane_harlequin_fx = GetModConfigData("CANE_HARLEQUIN_FX")
local cane_rose_fx      = GetModConfigData("CANE_ROSE_FX")

if not cane_ancient_fx then
    for i = 1, 3 do
        AddPrefabPostInit("cane_ancient_fx"..tostring(i), function(inst)
            inst._complete = true
        end)
    end
end

if not cane_victorian_fx then
    AddPrefabPostInit("cane_victorian_fx", function(inst)
        GLOBAL.EmitterManager:RemoveEmitter(inst)
        inst:DoTaskInTime(0, inst.Remove)
    end)
end

if not cane_candy_fx then
    AddPrefabPostInit("cane_candy_fx", function(inst)
        GLOBAL.EmitterManager:RemoveEmitter(inst)
        inst:DoTaskInTime(0, inst.Remove)
    end)
end

if not cane_sharp_fx then
    AddPrefabPostInit("cane_sharp_fx", function(inst)
        GLOBAL.EmitterManager:RemoveEmitter(inst)
        inst:DoTaskInTime(0, inst.Remove)
    end)
end

if not cane_harlequin_fx then
    AddPrefabPostInit("cane_harlequin_fx", function(inst)
        GLOBAL.EmitterManager:RemoveEmitter(inst)
        inst:DoTaskInTime(0, inst.Remove)
    end)
end

if not cane_rose_fx then
    AddPrefabPostInit("cane_rose_fx", function(inst)
        GLOBAL.EmitterManager:RemoveEmitter(inst)
        inst:DoTaskInTime(0, inst.Remove)
    end)
end
